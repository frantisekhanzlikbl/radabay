import { CSSObject } from "@emotion/css";

type Color = string;
type Background = string;
type Styles = CSSObject;

export interface Theme {
	general: {
		layout: {
			heading: {
				backgroundColor: Color;
				textStyles: Styles;
				borderColor: Color
			};
		};
		button: {
			textColor: Color;
			backgroundColor: Color
		}
	};
	pages: {
		index: {
			section1: {background: Background};
			section2: {
				textColor: Color,
				background: Background
			};
		};
	};
}
