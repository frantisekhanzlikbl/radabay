import styled from "@emotion/styled";
import React from "react";
import { fonts } from "../../../config/styles";

const HeadingContainer = styled.div`
	position: absolute;
	width: 100%;

	display: flex;
	justify-content: center;
	align-items: center;

	background-color: ${({ theme }) => theme.general.layout.heading.backgroundColor};

	border-bottom:2px solid ${({ theme }) => theme.general.layout.heading.borderColor};
`;

const Heading = styled.h1`
	margin: 0;
	padding: 1rem;

	display: inline;

	${fonts.caveat[700]}
	font-size: 4rem;
	text-align: center;
	line-height: 1;

	background-clip: text;
	-webkit-text-fill-color: transparent;
	-webkit-box-decoration-break: clone;
	box-decoration-break: clone;

	${({ theme }) => theme.general.layout.heading.textStyles}
`;

export const Header: React.FC = () => (
	<HeadingContainer>
		<Heading>
			Vítejte na oficiálních stránkách dětského dne Prestižov!
		</Heading>
	</HeadingContainer>
);
