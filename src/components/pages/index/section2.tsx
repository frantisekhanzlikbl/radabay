import styled from "@emotion/styled";
import React from "react";
import { fonts } from "../../../config/styles";
import { SkewPadding } from "../../common/skew container/padding";
import { skewAngleRad } from ".";

export const Container = styled.section`
	height: 20rem;
	display: flex;
	justify-content: center;

	color: ${({ theme }) => theme.pages.index.section2.textColor};
	background-color: ${({ theme }) => theme.pages.index.section2.background};
`;

const Content = styled(SkewPadding)`
	/*
		yes, I know that this solution is ugly,
		but I don't have the time to do it any better.
		maybe once I'm done with the homewrok.

		!TODO(potential_bug): implement a correct component for calculation padding of children under a skew container
	*/
	padding-top: 8rem;
`;

const FormContainer = styled.form`Container
	width: 100%;
	display: flex;
	flex-direction:column;
	gap: 2rem;
`;

const Inputs = styled.div`
	display: grid;
	grid-template-columns: 3fr 2fr;
	gap: 0.5rem;
`;

const Submit = styled.input`
	height: 2rem;

	${fonts.nunito[600]}
	letter-spacing: .2rem;
	color: ${({ theme }) => theme.general.button.textColor};
	background-color: ${({ theme }) => theme.general.button.backgroundColor};
	border: none;
	border-radius: 0.5rem;
`;

const Form: React.FC<{
	inputs: {
		id: string,
		label: string,
		inputProps: JSX.IntrinsicElements["input"]
	}[]
}> = ({ inputs }) => (
	<FormContainer>
		<Inputs>
			{inputs.map(({ id, label, inputProps }) => (
				<>
					<label htmlFor={id}>
						{label}
					</label>
					<input name={id} id={id} {...inputProps}/>
				</>
			))}
		</Inputs>
		<Submit type="submit" value="ZAREGISTROVAT" />
	</FormContainer>
);

export const Section2: React.FC = () => (
	<Container>
		<Content
			angleRad={
				skewAngleRad
			}
			width="calc(20rem + 20vw)"
			top
		>
			<Form
				inputs={[
					{ id: "name", label: "Jméno", inputProps: { type: "text", placeholder: "Leo Beránek" } },
					{ id: "email", label: "E-Mail", inputProps: { type: "email", placeholder: "leo.beran@hustymail.yo" } },
					{ id: "attendee_count", label: "Počet účastníků", inputProps: { type: "number", defaultValue: 1, min: 1, max: 20 } },
				]} />
		</Content>
	</Container>
);
