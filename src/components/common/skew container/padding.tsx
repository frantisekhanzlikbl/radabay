import styled from "@emotion/styled";
import React from "react";
import { Stylable } from "../../../models/react";
import { getSkewPadding } from ".";

interface CommonProps {
	angleRad: number,
	width: string,
}

const Container = styled.div<CommonProps & {
	top: string,
	bottom: string,
}>`
	width: ${({ width }) => width};

	padding-top: ${({ top }) => top};
	padding-bottom: ${({ bottom }) => bottom};
`;

export const SkewPadding: React.FC<Stylable & CommonProps & {
	top?: boolean
	bottom?: boolean
}> = ({ children, className, angleRad, width, top, bottom }) => {
	const padding = getSkewPadding({ angleRad, width });

	return (
		<Container
			top={top === true ? padding : "0"}
			bottom={bottom === true ? padding : "0"}
			{...{ className, angleRad, width }}>
			{children}
		</Container>
	);
};
