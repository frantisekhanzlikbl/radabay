import { CSSInterpolation } from "@emotion/css";
import { css } from "@emotion/react";
import styled from "@emotion/styled";
import React, { useMemo } from "react";
import { Stylable } from "../../../models/react";

// based on calculation explained at
// https://9elements.com/blog/pure-css-diagonal-layouts/#v-place-your-content-without-fear-
const getSkewPaddingCoefficient = (angleRad: number) => Math.abs(Math.tan(angleRad) / 2);

export const getSkewPadding = (
	{ angleRad, width }: {angleRad:number, width: string}
): string => `calc(${width} * ${getSkewPaddingCoefficient(angleRad)})`;

interface Props {
	angleRad: number;
	width: string;
	backgroundColor: string;
	skewContainerStyles?: CSSInterpolation;
}

const Container = styled.div<Props>(({ angleRad, width, backgroundColor, skewContainerStyles: skewContainerClass }) => {
	const padding = useMemo(() => getSkewPadding({ angleRad, width }), [angleRad, width]);

	return css`
		position: relative;

		margin: calc(0px - ${padding}) 0;

		::before {
			content: " ";

			position: absolute;
			inset: 0;

			display: block;
			height: 100%;

			background-color: ${backgroundColor};
			transform: skewY(${angleRad}rad);

			${skewContainerClass}
		}
	`;
});

const Content = styled.div`
	position: relative;
`;

export const SkewContainer: React.FC<
	Stylable & Props
> = ({ children, ...containerProps }) => (
	<Container {...containerProps}>
		<Content>
			{children}
		</Content>
	</Container>
);
